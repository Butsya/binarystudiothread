import {
    SET_USERS,
} from './mutationTypes';
import { SET_LOADING } from '../../mutationTypes';
import api from '@/api/Api';
import { userMapper } from '@/services/Normalizer';

export default {
    async fetchUsers({ commit }, { page }) {
        debugger;
        commit(SET_LOADING, true, { root: true });

        try {
            const users = await api.get(`/users`, { page });

            commit(SET_USERS, users);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(
                users.map(userMapper)
            );
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
};
