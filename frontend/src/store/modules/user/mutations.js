import {
    SET_USERS
} from './mutationTypes';
import { userMapper } from "@/services/Normalizer";

export default {
    [SET_USERS]: (state, users) => {
        state.users = {
            ...state.users,
            ...users.reduce(
                (prev, user) => ({ ...prev, [user.id]: userMapper(user) }),
                {}
            ),
        };
    },
};
