<?php

declare(strict_types=1);

namespace App\Action\Comment;


class UpdateCommentRequest
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $body;

    /**
     * @param int $id
     * @param string|null body
     */
    public function __construct(int $id, ?string $body)
    {
        $this->id = $id;
        $this->body = $body;
    }

    /**
     * Return comment id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Return comment body.
     *
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }
}
