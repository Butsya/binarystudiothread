<?php
declare(strict_types=1);

namespace App\Notifications;

use App\Entity\Comment;
use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommentLiked extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Comment
     */
    private $comment;

    public function __construct(User $user, Comment $comment)
    {
        $this->user = $user;
        $this->comment = $comment;
    }

    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    public function toArray($notifiable)
    {
        return [
            'comment_id' => $this->comment->getId(),
            'comment_body' => $this->comment->getBody(),
            'user_nickname' => $this->user->getNickName(),
        ];
    }
}
