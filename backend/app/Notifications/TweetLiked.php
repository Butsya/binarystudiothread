<?php

namespace App\Notifications;

use App\Entity\Tweet;
use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TweetLiked extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Tweet
     */
    private $tweet;

    public function __construct(User $user, Tweet $tweet)
    {
        $this->user = $user;
        $this->tweet = $tweet;
    }

    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    public function toArray($notifiable)
    {
        return [
            'tweet_id' => $this->tweet->getId(),
            'tweet_text' => $this->tweet->getText(),
            'user_nickname' => $this->user->getNickName(),
        ];
    }
}
